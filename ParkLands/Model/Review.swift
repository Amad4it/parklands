//
//  Review.swift
//  ParkLands
//
//  Created by Jason Khong on 11/16/15.
//  Copyright © 2015 ApptivityLab. All rights reserved.
//

import Foundation

class Review {
    var dateCreated: NSDate
    var rating: Int {
        didSet {
            if rating < 1 {
                self.rating = 1
            } else if rating > 5 {
                self.rating = 5
            }
        }
    }
    
    init(rating: Int, date: NSDate = NSDate()) {
        if rating < 1 {
            self.rating = 1
        } else if rating > 5 {
            self.rating = 5
        } else {
            self.rating = rating
        }

        self.dateCreated = date
    }
}