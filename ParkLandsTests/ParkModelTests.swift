//
//  ParkModelTests.swift
//  ParkLands
//
//  Created by Jason Khong on 11/17/15.
//  Copyright © 2015 ApptivityLab. All rights reserved.
//

import XCTest
@testable import ParkLands

class ParkModelTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testParkInitialization() {
        let testPark: Park = Park(name: "Test Park")!
        XCTAssertNotNil(testPark, "Test park should not be nil")
        
        let unnamedPark: Park? = Park(name: "")
        XCTAssertNil(unnamedPark, "Park cannot be created without a name")
    }
    
    func testReviewInitialization() {
        let firstReview: Review = Review(rating: 1)
        XCTAssertNotNil(firstReview, "Review object should not be nil")
        
        let secondReview: Review = Review(rating: 1)
        XCTAssertNotNil(secondReview, "Review object should not be nil")
        
        let cappedReview: Review = Review(rating: 99)
        XCTAssert(cappedReview.rating == 5, "Review's rating should be capped at 5")
    }
    
    func testAddingReviewsToPark() {
        let testPark: Park = Park(name: "Test Park")!

        let firstReview: Review = Review(rating: 1)
        let secondReview: Review = Review(rating: 1)
        
        testPark.reviews.append(firstReview)
        testPark.reviews.append(secondReview)
        
        let averageRating = testPark.averageRating()
        let expectedRating: Double = 1.0
        XCTAssertEqual(averageRating, expectedRating, "Average ratings should match expected average of 1.0")
    }
    
    func testExample() {
        let actual: Int = 1 + 1
        let expected: Int = 2
        
        XCTAssert(actual == expected, "1 + 1 should be equal to 2")
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
